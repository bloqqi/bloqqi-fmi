FMI support for Bloqqi programs
===============================

To generate an FMU:

	./build_fmu <path-to-bloqqi-program>

The FMU will be generated in the directory `fmu/`. This command requires `gcc` and `java`.

Description
-----
How FMUs are generated for Bloqqi programs are described in the following paper:

* Niklas Fors, Joel Petersson, Maria Henningsson. **Generating FMUs for the Feature-Based Language Bloqqi**. 2nd Japanese Modelica Conference, Tokyo, Japan, May 17-18, 2018. ([pdf](https://bloqqi.org/pdf/bloqqi-fmu.pdf))

Cross-compilation to Windows
------------------------

Use the flag `-w` to cross-compile to Windows 64 bits:

	./build_fmu <path-to-bloqqi-program> -w

This requires the cross-compiler [mingw-w64](https://mingw-w64.org/), which can be installed as follows:

	# On Ubuntu/Debian:
	$ apt install gcc-mingw-w64

	# On MacOS using brew:
	$ brew install mingw-w64


License
-------
This repository is covered by the license BSD 2-clause, see file LICENSE.

Credits
-------
The following files in the repository come from:

- `include/fmi2/`: Header files from the [FMI2 standard](http://fmi-standard.org/), licensed under BSD 2-clause.
- `include/fmusdk2/`: FMU SDK from [QTronic](https://www.qtronic.de/en/fmusdk.html), licensed under BSD 2-clause. 
- `build_fmu`: based on script from a [port of FMU SDK](https://github.com/cxbrooks/fmusdk2/) by Christopher Brooks, licensed under BSD 2-clause.